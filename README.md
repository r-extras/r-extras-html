# `r.extras.html`

## Extra functions and modules useful for `html` applications

### Installation from R console
>
> `> remotes::install_gitlab("r-extras/r-extras-html")`
>

### Dependency inclusion

Include also repository at `DESCRIPTION` file under `Remotes` entry.

```yaml
...
Imports:
    ...
    r.extras.html
...
Remotes:
    ...
    gitlab::r-extras/r-extras-html
...
```

### Package download

Created versions are available in the [package registry](https://gitlab.com/r-extras/r-extras-html/-/packages)

### Licence

SPDX [MIT](LICENCE) Licence.

***

# Release notes

 - `v0.0.2`: New base64 encoding and print functions. Increased exports