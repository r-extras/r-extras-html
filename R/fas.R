#' Font Awesome Solid icon
#'
#' Create a icon from Font Awesome Solid icon library.
#'  Code adapted from \code{fontawesome::fa_i}.
#'  Local dependency added with package.
#'
#' @seealso `add_fas_deps`
#'
#' @param name \code{character} font awesome icon name, without fa- prefix
#' @param class \code{character} additional classes character element
#' @param ... additional parameters passed to \code{htmltools::tags$i}
#' @return icon
#' @export
fas <- function(name, class = NULL, ...) {
    icon_class <- sprintf("fas fa-%s", name)
    if (!is.null(class)) {
        icon_class <- paste(icon_class, class)
    }
    icon_tag <- htmltools::tags$i(
        class = icon_class,
        role = "presentation",
        `aria-label` = paste(name, "icon"),
        ...
    )

    icon_tag <- add_fas_deps(icon_tag)
    icon_tag <- htmltools::browsable(icon_tag)
    return(icon_tag)
}
