#' Button to call \code{setDataTablesExport}
#' @param id \code{character} element id
#' @returns \code{tags$button}
#' @export
xlsxtable_button <- function(id = "xls-btn")
    htmltools::tags$button(
        id = id,
        class = "btn btn-info btn-sm pull-right no-print",
        onclick = "setDataTablesExport()",
        tags$i(class = "fa fa-file-excel-o")
    )

#' Convert elements of class table to DataTable on client side
#' @export
xlsxtable_js_text_to_head <- function()
    htmltools::tags$script(
    type = "text/javascript",
    "
        var s_xls = document.createElement('script');
        s_xls.innerText = \"var tbls = document.querySelectorAll('table');var i_ = 0; for (var i of tbls) {i.id = 'table_' + i_; i_++ }; var setDataTablesExport = function() {for (t of tbls) {$('#'+t.id).DataTable({dom: 'Btr', paging: false, ordering: false, buttons: ['excel', 'pdf']});}}\"
        s_xls.type = 'text/javascript';
        s_xls.defer = true;
        document.head.appendChild(s_xls);
    "
    )
